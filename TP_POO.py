#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math

class Point:
    """ classe permettant de représenter les points du plan rapportés
    à une origine fixée"""
    def __init__(self, x, y):
        self.__x = x
        self.__y = y
        
    def x(self):
        return self.__x
    
    def y(self):
        return self.__y
    
    def r(self):
        return math.sqrt(self.x()**2 + self.y()**2) 
    
    def t(self):
        return math.atan2(self.y(), self.x())
    
    def __str__(self):
        return '(' + str(self.x()) + ',' + str(self.y()) + ')'
    
    def __eq__(self, b):
        return isinstance(b, Point) and self.x() == b.x() and self.y() == b.y()
    
    def homothetie(self, k):
        self.__x = self.x() * k
        self.__y = self.y() * k
    
    def translation(self, dx, dy):
        self.__x = self.x() + dx
        self.__y = self.y() + dy
        
    def rotation(self, a):
        t = self.t() + a
        self.__x = self.r() * math.cos(t)
        self.__y = self.r() * math.sin(t)
        
        
p = Point(4, 6)
p.homothetie(4)
print(p)
p.translation(5,8)
print(p)
p.rotation(7)
print(p)


class LignePol:
    "représentation de lignes polygonales"
    def __init__(self, *sommets):
        self.__sommets = [*sommets]
        self.__nb_sommets = len(self.__sommets)
        
    def sommets(self):
        return self.__sommets
    
    def nb_sommets(self):
        return self.__nb_sommets
    
    def __str__(self):
        liste = ''
        for i in self.sommets():
            liste = liste + str(i)
        return 'Coordonnées de la ligne polygonale: ' + liste
        
    def get_sommets(self, i):
        return str(self.__sommets[i])
    
    def set_sommets(self, i, p):
        self.__sommets[i] = p
        return str(self.__sommets[i])
        
    def homothetie(self, k):
        for i in self.__sommets:
            i.homothetie(k)

    def translation(self,dx, dy):
        for i in self.__sommets:
            i.translation(dx,dy)
    
    def rotation(self, a):
        for i in self.__sommets:
            i.rotation(a)
        
        
q=LignePol(Point(8, 5),Point(9, 15),Point(18,16))
print(q)
q.homothetie(4)
print(q)
print(q.get_sommets(1))
q.translation(2,3)
print(q)
q.rotation(5)
print(q)



class Rectangle:
    def __init__(self, x0, y0, x1, y1):
        self.coin_se = Point(x0, y0)
        self.coin_no = Point(x1, y1)
    
    def __str__(self):
        return '[' + str(self.coin_se) + ' ; ' + str(self.coin_no) + ']'

if __name__ == '__main__':
    r1 = Rectangle(1, 2, 3, 4)
    r2 = r1
    print("r1: " + str(r1) + ", r2: " + str(r2))
    r1.coin_no = Point(0,0)
    print("r1: " + str(r1) + ", r2: " + str(r2))
    r1.coin_se.homothetie(2)
    print("r1: " + str(r1) + ", r2: " + str(r2))

# Le résultat montre qu'en faisant r1 = r2 on ne crée pas une copie de l'objet
# mais qu'on crée une nouvelle instance qui pointe sur le même objet
    
    import copy
    r1 = Rectangle(1, 2, 3, 4)
    r2 = copy.copy(r1)
    print("r1: " + str(r1) + ", r2: " + str(r2))
    r1.coin_no = Point(0,0)
    print("r1: " + str(r1) + ", r2: " + str(r2))
    r1.coin_se.homothetie(2)
    print("r1: " + str(r1) + ", r2: " + str(r2))

# en faisant copie, une modification de coin_no ne s'applique qu'à r1. On a donc
# bien crée un nouvel objet avec les coordonnées coin_no et coin_se présents dans
# la classe Rectangle. Par contre l'homothetie présente dans Point s'applique
# toujours sur r1 et r2 et pas uniquement sur r1
    
    r1 = Rectangle(1, 2, 3, 4)
    r2 = copy.deepcopy(r1)
    print("r1: " + str(r1) + ", r2: " + str(r2))
    r1.coin_no = Point(0,0)
    print("r1: " + str(r1) + ", r2: " + str(r2))
    r1.coin_se.homothetie(2)
    print("r1: " + str(r1) + ", r2: " + str(r2))

# deepcopy a permis de copier intégralement l'objet r1 pour créer un nouvel objet
# r2 avec les même caractéristiques
    