#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math

class Point:
    """ classe permettant de représenter les points du plan rapportés
    à une origine fixée"""
    def __init__(self, r, t):
        self.__r = r
        self.__t = t
        
    def r(self):
        return self.__r 
    
    def t(self):
        return self.__t
            
    def x(self):
        return math.cos(self.t())*self.r()
    
    def y(self):
        return math.sin(self.t())*self.r()
    
    def __str__(self):
        return '(' + str(self.r()) + ',' + str(self.t()) + ')'
    
    def __eq__(self, b):
        return isinstance(b, Point) and self.x() == b.x() and self.y() == b.y()
    
    def homothetie(self, k):
        self.__x = self.x() * k
        self.__y = self.y() * k
    
    def translation(self, dx, dy):
        self.__x = self.x() + dx
        self.__y = self.y() + dy
        
    def rotation(self, a):
        t = self.t() + a
        self.__x = self.r() * math.cos(t)
        self.__y = self.r() * math.sin(t)
        
        
p = Point(4, 6)
p.homothetie(4)
print(p)
p.translation(5,8)
print(p)
p.rotation(7)
print(p)


class LignePol:
    "représentation de lignes polygonales"
    def __init__(self, *sommets):
        self.__sommets = [*sommets]
        self.__nb_sommets = len(self.__sommets)
        
    def sommets(self):
        return self.__sommets
    
    def nb_sommets(self):
        return self.__nb_sommets
    
    def __str__(self):
        liste = ''
        for i in self.sommets():
            liste = liste + str(i)
        return 'Coordonnées de la ligne polygonale: ' + liste
        
    def get_sommets(self, i):
        return str(self.__sommets[i])
    
    def set_sommets(self, i, p):
        self.__sommets[i] = p
        return str(self.__sommets[i])
        
    def homothetie(self, k):
        for i in self.__sommets:
            i.homothetie(k)

    def translation(self,dx, dy):
        for i in self.__sommets:
            i.translation(dx,dy)
    
    def rotation(self, a):
        for i in self.__sommets:
            i.rotation(a)
        
        
q=LignePol(Point(8, 5),Point(9, 15),Point(18,16))
print(q)
q.homothetie(4)
print(q)
print(q.get_sommets(1))
q.translation(2,3)
print(q)
q.rotation(5)
print(q)
